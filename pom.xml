<?xml version="1.0" encoding="utf-8"?>
<!--
 ~ Copyright (c) WSO2 Inc. (http://wso2.com) All Rights Reserved.
 ~
 ~ Licensed under the Apache License, Version 2.0 (the "License");
 ~ you may not use this file except in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~      http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing, software
 ~ distributed under the License is distributed on an "AS IS" BASIS,
 ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ~ See the License for the specific language governing permissions and
 ~ limitations under the License.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <!--parent>
        <groupId>org.wso2.carbon</groupId>
        <artifactId>carbon-kernel</artifactId>
        <version>4.4.10</version>
        <relativePath>../pom.xml</relativePath>
    </parent-->



    <modelVersion>4.0.0</modelVersion>
    <groupId>org.wso2.carbon</groupId>
    <artifactId>org.wso2.carbon.user.core</artifactId>
    <packaging>jar</packaging>
    <name>WSO2 Carbon - User Manager Kernel</name>
    <description>A custom wso2 product</description>
    <version>4.4.10</version>
    <url>http://wso2.org</url>

    <prerequisites>
        <maven>3.0.0</maven>
    </prerequisites>

    <pluginRepositories>
        <pluginRepository>
            <id>wso2-nexus</id>
            <name>WSO2 internal Repository</name>
            <url>http://maven.wso2.org/nexus/content/groups/wso2-public/</url>
            <releases>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
                <checksumPolicy>ignore</checksumPolicy>
            </releases>
        </pluginRepository>
    </pluginRepositories>

    <repositories>
        <repository>
            <id>wso2-nexus</id>
            <name>WSO2 internal Repository</name>
            <url>http://maven.wso2.org/nexus/content/groups/wso2-public/</url>
        </repository>
    </repositories>


    <dependencies>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>javax.cache.wso2</artifactId>
            <version>4.4.10</version>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.user.api</artifactId>
            <version>4.4.10</version>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.utils</artifactId>
            <version>4.4.10</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
            <version>4.11</version>
        </dependency>
        <dependency>
            <groupId>org.wso2.orbit.com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>test</scope>
            <version>1.3.175.wso2v1</version>
        </dependency>
        <dependency>
            <groupId>commons-dbcp.wso2</groupId>
            <artifactId>commons-dbcp</artifactId>
            <version>1.4.0.wso2v1</version>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon</groupId>
            <artifactId>org.wso2.carbon.logging</artifactId>
            <version>4.4.10</version>
        </dependency>
        <dependency>
            <groupId>org.apache.tomcat.wso2</groupId>
            <artifactId>jdbc-pool</artifactId>
            <version>7.0.34.wso2v2</version>
        </dependency>
        <dependency>
            <groupId>org.wso2.securevault</groupId>
            <artifactId>org.wso2.securevault</artifactId>
            <version>1.0.0-wso2v2</version>
        </dependency>
        <dependency>
            <groupId>commons-pool.wso2</groupId>
            <artifactId>commons-pool</artifactId>
            <scope>test</scope>
            <version>1.5.6.wso2v1</version>
        </dependency>
        <dependency>
            <groupId>backport-util-concurrent.wso2</groupId>
            <artifactId>backport-util-concurrent</artifactId>
            <scope>test</scope>
            <version>3.1.0.wso2v1</version>
        </dependency>
        <dependency>
            <groupId>org.wso2.orbit.commons-collections</groupId>
            <artifactId>commons-collections</artifactId>
            <version>3.2.2.wso2v1</version>
        </dependency>
    </dependencies>

    <properties>
        <maven.compiler.source>1.7</maven.compiler.source>
        <maven.compiler.target>1.7</maven.compiler.target>
    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
            </plugin>
            <plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-resources</id>
                        <!-- here the phase you need -->
                        <phase>process-test-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${basedir}/target/clear-resources</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>src/test/resources</directory>
                                    <includes>
                                        <include>**/user-mgt-clear.xml</include>
                                        <include>**/WSO2CARBON_DB_CLEAR.h2.db</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!--plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <inherited>true</inherited>
                <configuration>
                    <excludes>
                        <exclude>**/BaseTestCase.java</exclude>
                    </excludes>
                </configuration>
            </plugin-->
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>findbugs</id>
            <activation>
                <property>
                    <name>findbugs</name>
                    <value>true</value>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>findbugs-maven-plugin</artifactId>
                        <version>2.0.1</version>
                        <configuration>
                            <effort>Max</effort>
                            <threshold>Low</threshold>
                            <xmlOutput>false</xmlOutput>
                            <excludeFilterFile>user-core-findbugs-excluded.xml</excludeFilterFile>
                        </configuration>
                        <executions>
                            <execution>
                                <phase>verify</phase>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>


</project>
