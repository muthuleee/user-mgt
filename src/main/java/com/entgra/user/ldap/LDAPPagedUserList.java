package com.entgra.user.ldap;

import com.entgra.user.PagedUserList;

import java.util.ArrayList;

public class LDAPPagedUserList implements PagedUserList {

    private LDAPPageControl control = null;
    private ArrayList<String> users = null;
    private boolean hasMorePages = false;

    public LDAPPagedUserList(ArrayList<String> users, LDAPPageControl control, boolean hasMorePages) {
        this.users = users;
        this.control = control;
        this.hasMorePages = hasMorePages;
    }

    public ArrayList<String> getUsers(){
        return users;
    }

    public LDAPPageControl getPageControl(){
        return control;
    }

    public boolean hasMorePages() {
        return hasMorePages;
    }

}
