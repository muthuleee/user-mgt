package com.entgra.user.ldap;


import com.entgra.user.PageControl;
import com.entgra.user.PageControlBuilder;
import com.entgra.user.PagedUserStoreManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.user.core.UserCoreConstants;
import org.wso2.carbon.user.core.UserRealm;
import org.wso2.carbon.user.core.UserStoreException;
import org.wso2.carbon.user.core.claim.ClaimManager;
import org.wso2.carbon.user.api.RealmConfiguration;
import org.wso2.carbon.user.core.ldap.LDAPConstants;
import org.wso2.carbon.user.core.ldap.ReadOnlyLDAPUserStoreManager;
import org.wso2.carbon.user.core.profile.ProfileConfigurationManager;
import org.wso2.carbon.user.core.util.UserCoreUtil;


import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchResult;
import javax.naming.directory.SearchControls;
import javax.naming.ldap.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PagedReadOnlyLDAPUserStoreManager extends ReadOnlyLDAPUserStoreManager implements PagedUserStoreManager {

    public static String PAGE_SIZE = "";


    private static Log log = LogFactory.getLog(PagedReadOnlyLDAPUserStoreManager.class);

    public PagedReadOnlyLDAPUserStoreManager() {
        super();
    }

    public PagedReadOnlyLDAPUserStoreManager(RealmConfiguration realmConfig, Map<String, Object> properties,
                                             ClaimManager claimManager, ProfileConfigurationManager profileManager,
                                             UserRealm realm, Integer tenantId) throws UserStoreException {
        super(realmConfig, properties, claimManager, profileManager, realm, tenantId);
    }

    public PagedReadOnlyLDAPUserStoreManager(RealmConfiguration realmConfig, Map<String, Object> properties,
                                             ClaimManager claimManager, ProfileConfigurationManager profileManager,
                                             UserRealm realm, Integer tenantId, boolean skipInitData) throws UserStoreException {
        super(realmConfig, properties, claimManager, profileManager, realm, tenantId, skipInitData);
    }

    public PagedReadOnlyLDAPUserStoreManager(RealmConfiguration realmConfig, ClaimManager claimManager,
                                             ProfileConfigurationManager profileManager) throws UserStoreException {
        super(realmConfig, claimManager, profileManager);
    }


    public LDAPPagedUserList pagedListUsers(String filter, PageControl pagedControl ) throws UserStoreException {

        LDAPPageControl ldapPageControl = (LDAPPageControl) pagedControl;

        if (filter.contains("?") || filter.contains("**")) {
            throw new UserStoreException(
                    "Invalid character sequence entered for user serch. Please enter valid sequence.");
        }

        int pageSize = Integer.parseInt(realmConfig.getUserStoreProperty(PAGE_SIZE));

        StringBuffer searchQuery = new StringBuffer(
                        realmConfig.getUserStoreProperty(LDAPConstants.USER_NAME_LIST_FILTER));
        String searchBases = realmConfig.getUserStoreProperty(LDAPConstants.USER_SEARCH_BASE);

        String userNameProperty = realmConfig.getUserStoreProperty(LDAPConstants.USER_NAME_ATTRIBUTE);

        String finalFilter = getFinalFilter(searchQuery.toString(), filter);

        try {
            LdapContext ctx = ldapPageControl.getLdapContext();
            byte[] cookie = ldapPageControl.getCookie();
            if (cookie == null) {
                ctx.setRequestControls(new Control[]{new PagedResultsControl(pageSize, Control.NONCRITICAL)});
            } else {
                ctx.setRequestControls(new Control[]{new PagedResultsControl(pageSize, cookie, Control.NONCRITICAL)});
            }
            SearchControls searchCtls = new SearchControls();
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);


            NamingEnumeration results = ctx.search(searchBases, finalFilter, searchCtls);

            ArrayList<String> users = new ArrayList<>();
            while (results != null && results.hasMore()) {
                SearchResult entry = (SearchResult) results.next();
                Attribute attr = entry.getAttributes().get(userNameProperty);

                if (attr != null) {
                    String name = (String) attr.get();
                    // append the domain if exist
                    String domain = getRealmConfiguration().getUserStoreProperty(UserCoreConstants.RealmConfig.PROPERTY_DOMAIN_NAME);
                    // domainName/userName
                    name = UserCoreUtil.getCombinedName(domain, name, null);
                    users.add(name);
                }
            }

            boolean hasMorePages = false;
            byte[] newCookie = null;
            Control[] controls = ctx.getResponseControls();
            if (controls != null) {
                for (int i = 0; i < controls.length; i++) {
                    if (controls[i] instanceof PagedResultsResponseControl) {
                        PagedResultsResponseControl prrc = (PagedResultsResponseControl) controls[i];
                        newCookie = prrc.getCookie();
                    }
                }
            } else {
                log.debug("No controls were sent from the server");
            }

            ldapPageControl.setCookie(newCookie);

            if (newCookie == null) {
                hasMorePages = false;
                log.debug("******* Cookie is null");
            } else {
                hasMorePages = true;
                log.debug("******* Cookie is not null");
            }

            LDAPPagedUserList list = new LDAPPagedUserList(users, ldapPageControl, hasMorePages);
            return list;

        } catch (NamingException e) {
            log.error(e.getMessage(), e);
            throw new UserStoreException();
        } catch (IOException ie) {
            log.error(ie.getMessage(), ie);
            throw new UserStoreException();
        }
    }

    private String getFinalFilter(String ldapQuery, String filter) {

        StringBuffer finalFilter = new StringBuffer();

        String userNameProperty = realmConfig.getUserStoreProperty(LDAPConstants.USER_NAME_ATTRIBUTE);
        //String[] returnedAtts = new String[]{userNameProperty};
        finalFilter.append("(&").append(ldapQuery).append("(").append(userNameProperty).append("=")
                .append(filter).append("))");
        return finalFilter.toString();

    }

    /// Test code
    public PagedReadOnlyLDAPUserStoreManager(org.wso2.carbon.user.core.config.RealmConfiguration config) {
        this.realmConfig = config;
    }

    public static void main(String[] args) throws Exception {

        org.wso2.carbon.user.core.config.RealmConfiguration realmConfig = new org.wso2.carbon.user.core.config.RealmConfiguration();
        Map<String, String> properties = new HashMap<>();
        properties.put(LDAPConstants.CONNECTION_NAME, "uid=admin,ou=system");
        properties.put(LDAPConstants.CONNECTION_PASSWORD, "secret");
        properties.put(LDAPConstants.CONNECTION_URL, "ldap://localhost:10389");
        properties.put(LDAPConstants.USER_NAME_LIST_FILTER, "(objectClass=person)");
        properties.put(LDAPConstants.USER_SEARCH_BASE, "ou=staff,dc=example,dc=com");
        properties.put(LDAPConstants.USER_NAME_ATTRIBUTE, "uid");
        properties.put(PAGE_SIZE, "10");
        properties.put(PageControlBuilder.PAGE_CONTROL_CLASS, "com.entgra.user.ldap.LDAPPageControl");
        realmConfig.setUserStoreProperties(properties);

        PagedReadOnlyLDAPUserStoreManager manager = new PagedReadOnlyLDAPUserStoreManager(realmConfig);

        //Above automatically happens from the framework.

        //Below is what you need to include in the IoT code
        //Use abstract method to build the PagedControl
        //If you write another paged control you don't have to change the code.
        PageControl pagedControl = null;
        try {
            pagedControl = PageControlBuilder.buildPageControl(realmConfig);

            LDAPPagedUserList userlist = null;
            do {
                userlist = manager.pagedListUsers("*", pagedControl);

                ArrayList<String> userNameList = userlist.getUsers();
                for(int i = 0; i < userNameList.size(); i++) {
                    System.out.println(userNameList.get(i));
                }

                System.out.println("*********");
            } while (userlist !=null && userlist.hasMorePages());

        }finally {
            pagedControl.cleanUp();
        }

    }

}
