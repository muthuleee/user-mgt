package com.entgra.user;

import org.wso2.carbon.user.core.UserStoreException;


public interface PagedUserStoreManager {

    public PagedUserList pagedListUsers(String filter, PageControl pagedControl) throws UserStoreException;

}
